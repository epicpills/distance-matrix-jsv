package com.bb.distancematrix;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.bb.distancematrix.restclient.DistanceMatrixService;
import com.bb.distancematrix.restclient.dtos.HistoryRequestDto;
import com.bb.distancematrix.restclient.dtos.HistoryRequestListDto;
import com.bb.distancematrix.restclient.dtos.RestClientParentResponseDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Controller
@SpringBootApplication
public class Main {

  private static final Logger LOGGER = Logger.getLogger(Main.class);

  @Value("${spring.datasource.url}")
  private String dbUrl;

  @Autowired
  private DistanceMatrixService distanceMatrixService;

  public static void main(String[] args) throws Exception {
    SpringApplication.run(Main.class, args);
  }

  @RequestMapping("/")
  public String index() {
    LOGGER.info("Executing index controller method");
    return "index";
  }
  
  @RequestMapping(value="/getDistanceAndDuration", method = RequestMethod.GET)
  public ResponseEntity<RestClientParentResponseDto> getDistanceAndDuration(@RequestParam(value="origins") String origins, 
  																																					@RequestParam(value="destinations") String destinations,
  																																					@RequestParam(value="insertondb") Boolean insertRegistryOnHistoryDB,
  																																					@RequestParam(value="mode") String mode) throws JsonProcessingException {

  		LOGGER.info("Executing getDistanceAndDuration controller method");

	  	RestClientParentResponseDto restClientResponse = distanceMatrixService.getDistanceAndDuration(origins,destinations, mode, insertRegistryOnHistoryDB);

      return new ResponseEntity<RestClientParentResponseDto>(restClientResponse, HttpStatus.OK);
  }
  
  @RequestMapping(value="/getRequestsHistory", method = RequestMethod.GET)
  public ResponseEntity<ArrayList<HistoryRequestDto>> getRequestsHistory() {

  		LOGGER.info("Executing getRequestsHistory controller method");

	  	ArrayList<HistoryRequestDto> historyRequestsDto = distanceMatrixService.getRequestsHistoryRows();

      return new ResponseEntity<ArrayList<HistoryRequestDto>>(historyRequestsDto, HttpStatus.OK);
  }
  

  @Bean
  public DataSource dataSource() throws SQLException {
    if (dbUrl == null || dbUrl.isEmpty()) {
      return new HikariDataSource();
    } else {
      HikariConfig config = new HikariConfig();
      config.setJdbcUrl(dbUrl);
      return new HikariDataSource(config);
    }
  }

}
