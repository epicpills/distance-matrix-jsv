package com.bb.distancematrix.restclient;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.bb.distancematrix.restclient.dtos.DistanceMatrixResponseDto;
import com.bb.distancematrix.restclient.dtos.Element;
import com.bb.distancematrix.restclient.dtos.HistoryRequestDto;
import com.bb.distancematrix.restclient.dtos.RestClientParentResponseDto;

/**
 * Rest client to access distance matrix google API
 * Use an injected class HistoryTableManager for DB access
 * @author JSV
 */
@Service
public class DistanceMatrixClientImpl implements DistanceMatrixService{

	private static final Logger LOGGER = Logger.getLogger(DistanceMatrixClientImpl.class);
	private String baseUrl = "https://maps.googleapis.com/maps/api/distancematrix/json";
	
	@Autowired
	private HistoryTableManager historyTableManager;
	
	@Override
	public RestClientParentResponseDto getDistanceAndDuration(String origins, String destinations, 
																													  String mode, Boolean insertRegistryOnHistoryDB) {
		
	  	String url = baseUrl + "?units=decimal&mode=" + mode + "&origins="+ origins + "&destinations=" + destinations + "&key=AIzaSyCull_OMPV77_6NssEYPLFQj496rCaWzNg";
			LOGGER.info("getDistanceAndDuration Calling url: " + url);
      DistanceMatrixResponseDto distanceMatrixDto = null;

      try {

          distanceMatrixDto = callToDistanceMatrixGoogleApi(url);
          if (insertRegistryOnHistoryDB) tryToInsertRequestIntoHistory(origins,destinations);
          return convertToClientResponseDto(distanceMatrixDto);

      } catch (RestClientException e) {
      		LOGGER.error("Unable to resolve google api call due to: " + e.getMessage());
    	  	return new RestClientParentResponseDto("Unable to resolve google api call due to: " + e.getMessage(), false);
      } catch (Exception e) {
          return parseErrorResponseAndMapIntoClientResponseDto(distanceMatrixDto);
      }
	}

	@Override
	public ArrayList<HistoryRequestDto> getRequestsHistoryRows() {
		try {
				return historyTableManager.getRequestsHistoryRows();
		} catch (Exception e) {
    	  LOGGER.error("Error on getRequestsHistoryRows due to: " + e.getMessage());
    	  return new ArrayList<HistoryRequestDto>(0);
		}
	}

	
	private DistanceMatrixResponseDto callToDistanceMatrixGoogleApi(String url) throws RestClientException{
        return new RestTemplate().getForObject(url, DistanceMatrixResponseDto.class);
	}
	
	private void tryToInsertRequestIntoHistory(String origins, String destinations) {
      try { 
          historyTableManager.insertRequestIntoHistory(origins, destinations);
			} catch (Exception e) {
					LOGGER.error("#### Error inserting on request history due to " + e.getMessage());
			}
	}

	private RestClientParentResponseDto convertToClientResponseDto( DistanceMatrixResponseDto distanceMatrixDto) throws Exception {

    	  Element firstElement = distanceMatrixDto.getRows().get(0).getElements().get(0);
    	  String distance = firstElement.getDistance().getText();
    	  String duration = firstElement.getDuration().getText();
    	  
    	  String timeAndDistanceMsg = " Distance is: " + distance + " and Estimated Time is: " + duration;

    	  return new RestClientParentResponseDto(timeAndDistanceMsg, true);
	}

	private RestClientParentResponseDto parseErrorResponseAndMapIntoClientResponseDto(DistanceMatrixResponseDto distanceMatrixDto) {

				String parentStatus = "";
    	  String elementStatus = "";

				if (distanceMatrixDto != null) parentStatus = distanceMatrixDto.getStatus();

    	  try {
    	  		elementStatus = distanceMatrixDto.getRows().get(0).getElements().get(0).getStatus();
				} catch (Exception e1) {
            LOGGER.info("Unable to get element status due to: " + e1.getMessage());
				}
    	  String errorMsg =  "Unable to compute distance and time. Request status: " + parentStatus + " Element status: " + elementStatus + ""
    	  								 + ". Please refer to https://developers.google.com/maps/documentation/distance-matrix/intro for additional debug information.";  
    	  LOGGER.error(errorMsg);

    	  return new RestClientParentResponseDto(errorMsg, false);
	}
}