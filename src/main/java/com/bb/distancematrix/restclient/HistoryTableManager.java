package com.bb.distancematrix.restclient;

import java.util.ArrayList;

import com.bb.distancematrix.restclient.dtos.HistoryRequestDto;

public interface HistoryTableManager {

	void insertRequestIntoHistory(String distance, String duration) throws Exception;

	ArrayList<HistoryRequestDto> getRequestsHistoryRows() throws Exception;

}
