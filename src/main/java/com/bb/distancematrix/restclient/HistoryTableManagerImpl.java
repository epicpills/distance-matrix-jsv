package com.bb.distancematrix.restclient;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bb.distancematrix.restclient.dtos.HistoryRequestDto;

/**
 * This class isolates DB access methods to request_history table
 * @author JSV
 */
@Service
public class HistoryTableManagerImpl implements HistoryTableManager{

	private static final Logger LOGGER = Logger.getLogger(HistoryTableManagerImpl.class);

	@Autowired
	private DataSource dataSource;

	@Override
	public void insertRequestIntoHistory(String origin, String destination) throws Exception{

		try (Connection connection = dataSource.getConnection()) {

		  LOGGER.info("Connection is: " + connection);
		  Statement stmt = connection.createStatement();

		  createRequestTableIfNotExsists(stmt);
		  insertRequestIntoHistoryDBTable(origin, destination, stmt);

		} catch (Exception e){
			
    	  LOGGER.error("Exception on manageDB due to: " + e.getMessage());
    	  throw e;
		}
	}
	//TODO: This should be removed on production:
  private void createRequestTableIfNotExsists(Statement stmt) throws SQLException {
      stmt.executeUpdate("CREATE TABLE IF NOT EXISTS request_history("
																	+ "request_time timestamp,"
																	+ "origins varchar(40),"
																	+ "destinations varchar(40)"
																	+ ")");
  }

	private void insertRequestIntoHistoryDBTable(String origin, String destination, Statement stmt) throws SQLException {
			LOGGER.info("Inserting request into history DB Table" + " origin: " + origin + " destination: " + destination);
		  stmt.executeUpdate("INSERT INTO request_history (request_time, origins, destinations) VALUES (now(), '" + origin + "', '" + destination + "')");
	}
	
	@Override
	public ArrayList<HistoryRequestDto> getRequestsHistoryRows() throws Exception{

    LOGGER.info("Trying to get request history");

		try (Connection connection = dataSource.getConnection()) {

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM request_history order by request_time desc limit 10");

        ArrayList<HistoryRequestDto> historyRequests = new ArrayList<HistoryRequestDto>();

        while (rs.next()) {
        	historyRequests.add(mapDBResultIntoHistoryRequestDto(rs));
        }
        return historyRequests;

		} catch (Exception e){
    	  LOGGER.error("Exception on getRequestHistory due to: " + e.getMessage());
    	  throw e;
		}
	}

  private HistoryRequestDto mapDBResultIntoHistoryRequestDto(ResultSet rs) throws SQLException {
	  HistoryRequestDto historyRequestDto = new HistoryRequestDto();
	  historyRequestDto.setRequest_time(rs.getTimestamp("request_time"));
	  historyRequestDto.setOrigins(rs.getString("origins"));
	  historyRequestDto.setDestinations(rs.getString("destinations"));
	  return historyRequestDto;
  }
}