package com.bb.distancematrix.restclient.dtos;

import java.util.Date;

public class HistoryRequestDto {
	
	private Date request_time;
	private String origins;
	private String destinations;

	public Date getRequest_time() {
		return request_time;
	}
	public void setRequest_time(Date request_time) {
		this.request_time = request_time;
	}
	public String getOrigins() {
		return origins;
	}
	public void setOrigins(String origins) {
		this.origins = origins;
	}
	public String getDestinations() {
		return destinations;
	}
	public void setDestinations(String destinations) {
		this.destinations = destinations;
	}

}
