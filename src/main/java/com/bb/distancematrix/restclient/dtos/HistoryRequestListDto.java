package com.bb.distancematrix.restclient.dtos;

import java.util.ArrayList;

public class HistoryRequestListDto {

	public HistoryRequestListDto() {
	}

	public HistoryRequestListDto(ArrayList<HistoryRequestDto> historyRequests) {
		super();
		this.historyRequests = historyRequests;
	}

	private ArrayList<HistoryRequestDto> historyRequests ;

	public ArrayList<HistoryRequestDto> getHistoryRequests() {
		return historyRequests;
	}

	public void setHistoryRequests(ArrayList<HistoryRequestDto> historyRequests) {
		this.historyRequests = historyRequests;
	}
}
