package com.bb.distancematrix.restclient.dtos;

public class RestClientParentResponseDto {

	private String timeAndDistanceCalculated;
	private boolean requestSuccessful;

	public RestClientParentResponseDto(){
	}

	public RestClientParentResponseDto(String timeAndDistanceCalculated, boolean requestSuccessful) {
		super();
		this.timeAndDistanceCalculated = timeAndDistanceCalculated;
		this.requestSuccessful = requestSuccessful;
	}

	public String getTimeAndDistanceCalculated() {
		return timeAndDistanceCalculated;
	}
	public void setTimeAndDistanceCalculated(String timeAndDistanceCalculated) {
		this.timeAndDistanceCalculated = timeAndDistanceCalculated;
	}


	public boolean isRequestSuccessful() {
		return requestSuccessful;
	}

	public void setRequestSuccessful(boolean requestSuccessful) {
		this.requestSuccessful = requestSuccessful;
	}

}
