package com.bb.distancematrix.restclient;

import java.util.ArrayList;

import com.bb.distancematrix.restclient.dtos.HistoryRequestDto;
import com.bb.distancematrix.restclient.dtos.RestClientParentResponseDto;

public interface DistanceMatrixService {

	RestClientParentResponseDto getDistanceAndDuration(String origins, String destinations, String mode, Boolean insertOnDb);

	ArrayList<HistoryRequestDto> getRequestsHistoryRows();


}
